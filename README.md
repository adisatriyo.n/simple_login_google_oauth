## Installation

This project show you how to Implement login feature using your google account in laravel8. Build with laravel jetstream and laravel socialite as package to help us connect with google auth api. Follow this command step by step on terminal to install on your computer.

- Run `git clone https://gitlab.com/adisatriyo.n/simple_login_google_oauth.git`
- Run `cd simple_login_google_oauth`
- Run `composer install`
- Run `npm install`
- Run `cp .env.example .env` (Linux) or `copy .env.example .env` (Windows cmd.exe)
- Run `php artisan key:generate`
- Run `php artisan cache:clear`
- Run `php artisan view:clear`
- Run `php artisan config:clear`
- Run `create empty database for your project`
- In the .env file, fill in the options DB_HOST, DB_PORT, DB_DATABASE, DB_USERNAME and DB_PASSWORD so that they match the credentials of the database you have just created.
- Run `php artisan migrate`
- Run `php artisan serve`
- go to http://localhost:8000/login

## Capture Application
- Login page
![Login Page](login1.PNG)
- After login with your google account go to the next page ex. dashboard page
![dashboard Page](afterLoginPage.PNG)
